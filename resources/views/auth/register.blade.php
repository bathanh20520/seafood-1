@extends('font_end.layouts.master')
@section('title', 'dang ky')
@section('css')
    <link rel="stylesheet" href="./assets/css/login.css" />
@endsection
@section('content')

<div style="max-width: 100%; width: 100%">
<div>
        <div class="breadcrumb-page">
          <div class="container">
            <ul class="breadcrumb-header">
              <li class="breadcrumb-header-item">
                <a href="#">Trang chủ</a>
              </li>
              <li class="breadcrumb-header-item">
                <a href="#">Tài khoản</a>
              </li>
              <li class="breadcrumb-header-item active">
                <span>Đăng ký</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="container">
          <div class="form-login mt-4">
            <h1>Đăng ký</h1>
            <p>Bạn đã có tài khoản? <a href="http://">Đăng nhập</a></p>
            <label> Tên:</label>
            <input type="text" placeholder="Tên" />
            <label> Họ </label>
            <input type="text" placeholder="Họ" />
            <label> Email:</label>
            <input type="text" placeholder="Email" />
            <label> Mật khẩu</label>
            <input type="text" placeholder="Mật khẩu" />
            <button class="btn">Đăng ký</button>
          </div>
        </div>
      </div>
</div>
@endsection
@extends('font_end.layouts.master')
@section('title', 'Gio hang')
@section('css')
    <link rel="stylesheet" href="./assets/css/detail_cart.css" />
@endsection
@section('content')
<div>
        <div class="breadcrumb-page">
          <div class="container">
            <ul class="breadcrumb-header">
              <li class="breadcrumb-header-item">
                <a href="#">Trang chủ</a>
              </li>

              <li class="breadcrumb-header-item active">
                <span>Giỏ hàng: 2 sản phẩm -208,000 </span>
              </li>
            </ul>
          </div>
        </div>
        <div class="cart-detail-wrapper">
          <div class="container">
            <div class="cart-detail-body">
              <div class="cart-detail active">
                <h4>Giỏ hàng</h4>
                <div class="cart-detail-content">
                  <!-- header -->
                  <div class="table-body header-cart">
                    <div class="row">
                      <div class="col-md-5">
                        <span>Sản phẩm</span>
                      </div>
                      <div class="col-md-7">
                        <div class="row">
                          <div class="col-md-3"><span>Giá</span></div>
                          <div class="col-md-3"><span>Số lượng</span></div>
                          <div class="col-md-3"><span>Tổng</span></div>
                          <div class="col-md-3"><span>Xóa</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--   -->
                  <div class="table-body active">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="border-cart">
                          <div class="row">
                            <div class="col-4">
                              <div class="image-product-cart">
                                <img
                                  src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_medium.jpg"
                                  alt=""
                                />
                              </div>
                            </div>
                            <div class="col-8">
                              <div class="title-product-title">
                                <a href="http://"
                                  >Cồi sò điệp <span>500g</span></a
                                >
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="cart-detail-child border-cart">
                          <div class="row">
                            <div class="col-3">
                              <span class="h4">Giá</span><span>128,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Số lượng</span>
                              <div>
                                <div class="number-pro clearfix">
                                  <input
                                    type="number"
                                    max="100"
                                    min="1"
                                    value="1"
                                  />
                                </div>
                              </div>
                            </div>
                            <div class="col-3">
                              <span class="h4">Tổng</span><span>128,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Xóa</span>
                              <a href="#" class="delete-cart"
                                ><i class="fa fa-trash" aria-hidden="true"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--   -->
                  <div class="table-body active">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="border-cart">
                          <div class="row">
                            <div class="col-4">
                              <div class="image-product-cart">
                                <img
                                  src="https://product.hstatic.net/1000181509/product/so-diep-1_medium.jpg"
                                  alt=""
                                />
                              </div>
                            </div>
                            <div class="col-8">
                              <div class="title-product-title">
                                <a href="http://">Hp-sò điệp </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="cart-detail-child border-cart">
                          <div class="row">
                            <div class="col-3">
                              <span class="h4">Giá</span><span>80,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Số lượng</span>
                              <div>
                                <div class="number-pro clearfix">
                                  <input
                                    type="number"
                                    max="100"
                                    min="1"
                                    value="1"
                                  />
                                </div>
                              </div>
                            </div>
                            <div class="col-3">
                              <span class="h4">Tổng</span><span>80,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Xóa</span>
                              <a href="#" class="delete-cart"
                                ><i class="fa fa-trash" aria-hidden="true"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--   -->
                  <div class="table-body active">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="border-cart">
                          <div class="row">
                            <div class="col-4">
                              <div class="image-product-cart">
                                <img
                                  src="https://product.hstatic.net/1000181509/product/ngao-2-coi-1_medium.jpg"
                                  alt=""
                                />
                              </div>
                            </div>
                            <div class="col-8">
                              <div class="title-product-title">
                                <a href="http://">Ngao 2 cồi</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="cart-detail-child border-cart">
                          <div class="row">
                            <div class="col-3">
                              <span class="h4">Giá</span><span>152,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Số lượng</span>
                              <div>
                                <div class="number-pro clearfix">
                                  <input
                                    type="number"
                                    max="100"
                                    min="1"
                                    value="1"
                                  />
                                </div>
                              </div>
                            </div>
                            <div class="col-3">
                              <span class="h4">Tổng</span><span>152,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Xóa</span>
                              <a href="#" class="delete-cart"
                                ><i class="fa fa-trash" aria-hidden="true"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--   -->
                  <div class="table-body active">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="border-cart">
                          <div class="row">
                            <div class="col-4">
                              <div class="image-product-cart">
                                <img
                                  src="https://product.hstatic.net/1000181509/product/so-mai-1_medium.jpg"
                                  alt=""
                                />
                              </div>
                            </div>
                            <div class="col-8">
                              <div class="title-product-title">
                                <a href="http://"
                                  >Sò mai</span></a
                                >
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="cart-detail-child border-cart">
                          <div class="row">
                            <div class="col-3">
                              <span class="h4">Giá</span><span>150,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Số lượng</span>
                              <div>
                                <div class="number-pro clearfix">
                                  <input
                                    type="number"
                                    max="100"
                                    min="1"
                                    value="1"
                                  />
                                </div>
                              </div>
                            </div>
                            <div class="col-3">
                              <span class="h4">Tổng</span><span>150,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Xóa</span>
                              <a href="#" class="delete-cart"
                                ><i class="fa fa-trash" aria-hidden="true"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--   -->
                  <div class="table-body active">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="border-cart">
                          <div class="row">
                            <div class="col-4">
                              <div class="image-product-cart">
                                <img
                                  src="https://product.hstatic.net/1000181509/product/tom_cang_xanh_01_medium.jpg"
                                  alt=""
                                />
                              </div>
                            </div>
                            <div class="col-8">
                              <div class="title-product-title">
                                <a href="http://"
                                  >Tôm càng xanh </span></a
                                >
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="cart-detail-child border-cart">
                          <div class="row">
                            <div class="col-3">
                              <span class="h4">Giá</span><span>340,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Số lượng</span>
                              <div>
                                <div class="number-pro clearfix">
                                  <input
                                    type="number"
                                    max="100"
                                    min="1"
                                    value="1"
                                  />
                                </div>
                              </div>
                            </div>
                            <div class="col-3">
                              <span class="h4">Tổng</span><span>340,000</span>
                            </div>
                            <div class="col-3">
                              <span class="h4">Xóa</span>
                              <a href="#" class="delete-cart"
                                ><i class="fa fa-trash" aria-hidden="true"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!--  -->
                </div>
                <div class="note-cart mt-4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="note-cart-left">
                        <label>Chú thích</label>
                        <textarea placeholder="Viết ghi chú"></textarea>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="note-cart-right">
                        <div class="total">
                          <span>Tổng : </span>
                          <span>720,000</span>
                        </div>
                        <a href="http://" class="btn">Tiếp tục mua sắm</a>
                        <a href="http://" class="btn">Thanh toán</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cart-detail" id="cart-detail-empty">
                <h4>Không có sản phẩm nào trong giỏ hàng của bạn</h4>
                <p>Tiếp tục mua hàng <a href="./product.html"> tại đây</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('script')
    <script src="./assets/js/detail_cart.js"></script>
@endsection
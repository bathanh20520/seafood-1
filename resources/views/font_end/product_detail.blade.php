@extends('font_end.layouts.master')
@section('css')
<link rel="stylesheet" href="./assets/css/product-detail.css" />
@endsection
@section('content')
<div style="max-width: 100%; width: 100%">
<div>
        <div class="breadcrumb-page">
          <div class="container">
            <ul class="breadcrumb-header">
              <li class="breadcrumb-header-item">
                <a href="#">Trang chủ</a>
              </li>
              <li class="breadcrumb-header-item">
                <a href="#">Sản phẩm khuyến mãi</a>
              </li>
              <li class="breadcrumb-header-item active">
                <span>Cồi sò điệp </span>
              </li>
            </ul>
          </div>
        </div>
        <div class="mt-5">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <div class="list-image-large clearfix">
                  <img
                    src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_master.jpg"
                    alt=""
                    id="zoom_image"
                    data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-1_master.jpg"
                  />
                  <div class="sale"><span>-20%</span></div>
                </div>
                <div class="list-image-small">
                  <div class="owl-carousel owl-theme" id="list-small">
                    <!-- 1 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-1_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-1_master.jpg"
                    >
                      <div class="active1">
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 2 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-2_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-2_master.jpg"
                    >
                      <div>
                        <img
                          id="zoom_image"
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-2_small.jpg"
                          alt="Cồi sò điệp"
                        />
                      </div>
                    </a>
                    <!-- 3 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-3_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-3_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-3_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 4 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-4_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-4_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-4_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 5 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-5_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-5_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-5_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 6 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-6_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-6_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-6_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 7 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-7_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-7_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-7_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 8 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-nau-khoai-tay-bi_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-nau-khoai-tay-bi_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-nau-khoai-tay-bi_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 9 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-nuong-sa-te_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-nuong-sa-te_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-nuong-sa-te_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!--10  -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-chanh-day_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-chanh-day_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-chanh-day_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 11 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-xo_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-xo_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-xo_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                    <!-- 12 -->
                    <a
                      href="#"
                      class="item-image-small"
                      data-image="https://product.hstatic.net/1000181509/product/coi-so-diep-xao-xa-ot_master.jpg"
                      data-zoom-image="https://product.hstatic.net/1000181509/product/coi-so-diep-xao-xa-ot_master.jpg"
                    >
                      <div>
                        <img
                          src="https://product.hstatic.net/1000181509/product/coi-so-diep-xao-xa-ot_small.jpg"
                          alt="Cồi sò điệp"
                          id="zoom_image"
                        />
                      </div>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-detail-product">
                  <h1>Cồi sò điệp</h1>
                  <div class="detail-price">
                    <span>128,000</span>
                    <div><label>Giá gốc :</label> <del>160,000</del></div>
                    <div><label> Giảm :</label><span> 32,000(-20%)</span></div>
                  </div>
                  <div class="weight-pro">
                    <label> Khối lượng</label>
                    <div class="select-info">
                      <label class="active"
                        ><input type="radio" name="weight" checked />500g
                        <span class="checkmark"></span
                      ></label>

                      <label
                        ><input type="radio" name="weight" />1kg
                        <span class="checkmark"></span
                      ></label>
                    </div>
                  </div>
                  <div>
                    <div class="number-button">
                      <label>Số lượng</label>
                      <div class="number-pro clearfix">
                        <input type="number" max="100" min="1" value="1" />
                      </div>
                    </div>
                    <div class="button-tool">
                      <button>Thêm vào giỏ</button>
                      <button>Mua ngay</button>
                    </div>
                    <div class="tags-pro">
                      <label>Tags:</label>
                      <a href="http://">Cồi sò điệp</a
                      ><a href="http://"> Ngao-Sò-Ốc</a>
                    </div>
                    <div class="share-pro clearfix">
                      <label> Chia sẻ:</label>
                      <ul>
                        <li class="clearfix">
                          <a href="http://"
                            ><i class="fa fa-twitter" aria-hidden="true"></i
                            >Twitter</a
                          >
                        </li>
                        <li>
                          <a href="http://"
                            ><i class="fa fa-facebook" aria-hidden="true"></i
                            >Facebook</a
                          >
                        </li>
                        <li>
                          <a href="http://"
                            ><i class="fa fa-google-plus" aria-hidden="true"></i
                            >Google+</a
                          >
                        </li>
                        <li>
                          <a href="http://"
                            ><i class="zb-logo-zalo"></i> Chia sẻ</a
                          >
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mt-4">
              <div class="col-md-9">
                <div>
                  <div class="tab">
                    <button class="tablinks active" data-link="detail">
                      chi tiết sản phẩm
                    </button>
                    <button class="tablinks" data-link="comment">
                      bình luận
                    </button>
                    <button class="tablinks" data-link="return">
                      chính sách đổi trả
                    </button>
                    <button class="tablinks" data-link="delivery">
                      chính sách giao hàng
                    </button>
                  </div>

                  <!-- Tab content -->
                  <div id="detail" class="tabcontent active">
                    <p>
                      - Cồi sò điệp là phần thịt được tách ra từ vỏ của sò điệp.
                      Cồi sò điệp là phần ngon và quý nhất của sò, thịt trắng,
                      dai, có vị giòn ngọt, tính hiền... Cồi sò điệp chứa nhiều
                      dưỡng chất, vitamin và khoáng chất tốt cho sức khỏe, tăng
                      sức đề kháng.
                    </p>
                    <p>
                      - Bản chất thịt cồi sò điệp ngọt thanh, mát nên các món ăn
                      tiếp nối ra đời. Với những nguyên liệu không cầu kỳ nhưng
                      làm ra những món ăn vô cùng đắt giá cho sức khỏe thì cồi
                      sò điệp rất được yêu thích.
                    </p>
                    <p>
                      <img
                        src="https://product.hstatic.net/1000181509/product/coi-so-diep-1_grande.jpg"
                        alt="Cồi sò điệp tươi"
                      />
                    </p>
                    <p class="note-image"><em> Cồi sò điệp tươi</em></p>
                    <p>
                      <img
                        src="https://product.hstatic.net/1000181509/product/coi-so-diep-5_grande.jpg"
                        alt="Cồi sò điệp tươi"
                      />
                    </p>
                    <p class="note-image"><em> Cồi sò điệp tươi</em></p>
                    <h2>
                      <span> <strong> Các món ăn từ cồi sò điệp </strong></span>
                    </h2>
                    <p><strong>1. Cồi sò điệp xào xả ớt</strong></p>
                    <p>
                      <img
                        src="https://product.hstatic.net/1000181509/product/coi-so-diep-xao-xa-ot_grande.jpg"
                        alt="Cồi sò điệp xào xả ớt"
                      />
                    </p>
                    <p class="note-image"><em> Cồi sò điệp xào xả ớt</em></p>
                    <p><strong>2. Cồi sò điệp nướng xa tế </strong></p>
                    <p>
                      <img
                        src="https://product.hstatic.net/1000181509/product/coi-so-diep-nuong-sa-te_grande.jpg"
                        alt="Cồi sò điệp nướng xa tế"
                      />
                    </p>
                    <p class="note-image"><em> Cồi sò điệp nướng xa tế</em></p>
                    <p><strong>3. Cồi sò điệp sốt chanh dây </strong></p>
                    <p>
                      <img
                        src="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-chanh-day_grande.jpg"
                        alt="Cồi sò điệp sốt chanh dây"
                      />
                    </p>
                    <p class="note-image">
                      <em> Cồi sò điệp sốt chanh dây</em>
                    </p>
                    <p><strong>4. Cồi sò điệp nấu khoai tây bí </strong></p>
                    <p>
                      <img
                        src="https://product.hstatic.net/1000181509/product/coi-so-diep-nau-khoai-tay-bi_grande.jpg"
                        alt="Cồi sò điệp nấu khoai tây bí"
                      />
                    </p>
                    <p class="note-image">
                      <em> Cồi sò điệp nấu khoai tây bí</em>
                    </p>
                    <p><strong> 5. Cồi sò điệp sốt XO</strong></p>
                    <p>
                      <img
                        src="https://product.hstatic.net/1000181509/product/coi-so-diep-sot-xo_grande.jpg"
                        alt=" Cồi sò điệp sốt XO"
                      />
                    </p>
                    <p class="note-image"><em> Cồi sò điệp sốt XO</em></p>
                  </div>

                  <div id="comment" class="tabcontent">
                    <div class="comment-content">
                      <div class="comment-item-tab">
                        <div class="number-comment">
                          <strong>0 Comments</strong>
                        </div>
                        <div></div>
                      </div>
                      <div class="input-comment">
                        <input
                          type="text"
                          placeholder="Thêm một bình luận... "
                        />
                      </div>
                      <div class="link-facebook">
                        <i class="fa fa-facebook-square" aria-hidden="true"></i
                        ><a href="http://"> Facebook Comments Plugin</a>
                      </div>
                    </div>
                  </div>

                  <div id="return" class="tabcontent">
                    <p>
                      Seafood chuyên cung cấp sỉ và lẻ các loại hải sản tươi
                      sống, hải sản đóng hộp với các sản phẩm chính sau:
                    </p>
                    <ul class="product-introduce">
                      <li>Tôm</li>
                      <li>Cá</li>
                      <li>Hàu</li>
                      <li>Bào Ngư</li>
                      <li>Ngao - Sò - Ốc</li>
                      <li>Mực</li>
                      <li>Chả cá và các loại hải sản khác......</li>
                    </ul>
                    <p>
                      Những sản phẩm cung cấp đến bạn là 100% từ thiên nhiên
                      luôn tươi ngon, sạch. Hầu hết các sản phẩm có nguồn gốc từ
                      các vùng biển Cam Ranh, Phan Thiết, Nha Trang....
                    </p>
                  </div>
                  <div id="delivery" class="tabcontent">
                    <p>
                      Seafood chuyên cung cấp sỉ và lẻ các loại hải sản tươi
                      sống, hải sản đóng hộp với các sản phẩm chính sau:
                    </p>
                    <ul class="product-introduce">
                      <li>Tôm</li>
                      <li>Cá</li>
                      <li>Hàu</li>
                      <li>Bào Ngư</li>
                      <li>Ngao - Sò - Ốc</li>
                      <li>Mực</li>
                      <li>Chả cá và các loại hải sản khác......</li>
                    </ul>
                    <p>
                      Những sản phẩm cung cấp đến bạn là 100% từ thiên nhiên
                      luôn tươi ngon, sạch. Hầu hết các sản phẩm có nguồn gốc từ
                      các vùng biển Cam Ranh, Phan Thiết, Nha Trang....
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div>
                  <div class="pro-view">
                    <h3>Sản phẩm cùng nhóm</h3>
                    <ul>
                      <!-- 1 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://">
                            <h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://">
                            <h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://"
                            ><h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://"
                            ><h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 5 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://"
                            ><h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 6 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://"
                            ><h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 7 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://">
                            <h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="pro-view">
                    <h3>sản phẩm đã xem</h3>
                    <ul>
                      <!-- 1 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://"
                            ><h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 2 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://">
                            <h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 3 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://">
                            <h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                      <!-- 4 -->
                      <li class="item-pro-view">
                        <div class="image-pro-item">
                          <a href="http://"
                            ><img
                              src="https://product.hstatic.net/1000181509/product/so-diep-1_small.jpg"
                              alt="hp-Sò điệp"
                          /></a>
                        </div>
                        <div class="info-pro-item">
                          <a href="http://">
                            <h4>hp-Sò điệp</h4>
                            <div class="price-item">
                              <span>80,000</span> <del>100,000</del>
                            </div></a
                          >
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
@section('script')
<script src="./assets/js/product_detail.js"></script>
@endsection
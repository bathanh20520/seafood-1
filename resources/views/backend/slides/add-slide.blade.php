@extends('backend.base')
@section('content')
        <!-- Phần viết code-->

        <div>
            <div class="card">
                <div class="card-header">
                    <strong>Thêm slide</strong>
                    <a href="{{route('slider')}}"><button type="submit" style="width: 100px"><i class="fa fa-mail-reply mr-sm-2"></i>Quay lại</button></a>
                </div>
                <div class="card-body card-block">
                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                @csrf
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">link</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="link" placeholder="Nhập tiêu đề" class="form-control" value="{{old('tieude')}}"></div>
                @if($errors->has('link'))
                    <div style="color:red; margin-left: 285px">{{ $errors->first('link') }}</div>
                @endif
                </div>
                  <div class="row form-group">
                    <div class="col col-md-3"><label for="file-input" class=" form-control-label">Ảnh</label></div>
                    <div class="col-12 col-md-9"><input type="file" id="addimg" name="image" class="form-control" placeholder="Nhập đường dẫn ảnh"></div>
                @if($errors->has('image'))
                    <div style="color:red; margin-left: 285px">{{ $errors->first('image') }}</div>
                @endif
                </div>
            
                <div class="card-footer">
                        <input class="btn btn-success" type="submit" value="Thêm">
                </div>
                </form>
                <div class="card-footer">
                <a href="add-slide.html">
                        <input class="btn btn-danger" type="submit" value="Hủy">
                    </a>
                </div>
                </div>  
            </div>
        </div>

    @endsection
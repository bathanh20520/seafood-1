@extends('backend.base')
@section('content')
        <!-- Phần viết code-->
            <div class="card">
                <div class="card-header">
                    <strong>Sửa sản phẩm</strong>
                    <a href="{{route('sanpham')}}"><button type="submit" style="width: 100px"><i class="fa fa-mail-reply mr-sm-2"></i>Quay lại</button></a>
                </div>
                <div class="card-body card-block">
                <form action="{{route('posteditsanpham',['id'=>$products->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                @csrf
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tên sản phấm</label></div>
                    <div class="col-12 col-md-9"><input value="{{$products->name}}" type="text" id="text-input" name="name" placeholder="Nhập tên sản phấm" class="form-control"></div>
                @if($errors->has('name'))
                    <div style="color:red; margin-left: 320px">{{ $errors->first('name') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="select" class=" form-control-label">Danh mục</label></div>
                    <div class="col-12 col-md-9">
                        <select name="cate_id" id="select" class="form-control" >
                        @foreach($list_cate as $cate)
                            @if($cate->id == $products->cate_id)
                            <option selected value="{{$products->cate_id}}">{{ $cate->name }}</option>
                            @else
                            <option value="{{$cate->id}}">{{ $cate->name }}</option>
                            @endif
                        @endforeach
                        </select>

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="file-input" class=" form-control-label">Ảnh</label></div>
                    <div class="col-12 col-md-9"><img src="{{ asset($products->image) }}" width="180px" height=""></div>
                    <div class="col-12 col-md-9" style="margin-left: 308px; margin-top: 10px;"><input value="{{$products->image}}" type="file" id="file-input" name="image" class="form-control-file"></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Giá sản phẩm</label></div>
                    <div class="col-12 col-md-9"><input value="{{$products->price}}" type="number" id="text-input" name="price" placeholder="Nhập giá sản phấm" class="form-control"></div>
                @if($errors->has('price'))
                    <div style="color:red; margin-left: 320px">{{ $errors->first('price') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Sale</label></div>
                    <div class="col-12 col-md-9"><input value="{{$products->sale_price}}" type="number" id="text-input" name="sale_price" placeholder="Nhập giá khuyến mãi" class="form-control"></div>
                @if($errors->has('sale_price'))
                    <div style="color:red; margin-left: 320px">{{ $errors->first('sale_price') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Mô tả</label></div>
                    <div class="col-12 col-md-9">
                    <textarea class="form-control" id="summary-ckeditor" name="description" >{{$products->description}}</textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Chi tiết</label></div>
                    <div class="col-12 col-md-9">
                    <textarea class="form-control" id="summary-ckeditor" name="detail">{{$products->detail}}</textarea>
                    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                    <script>
                    CKEDITOR.replace( 'detail' );
                    </script>
                    </div>
                </div>
                <div class="footer-add">
                        <input class="btn btn-success" type="submit" value="Sửa">
                </div>
                </form>
                <div class="footer-delete">
                <a href="">
                        <input class="btn btn-danger" type="submit" value="Hủy">
                    </a>
                </div>
                </div>  
            </div>
    @endsection
@extends('backend.base')
@section('content')
        <!-- Phần viết code-->

        <div>
            <div class="card">
                <div class="card-header">
                    <strong>Thêm sản phẩm</strong>
                    <a href="{{route('sanpham')}}"><button type="submit" style="width: 100px"><i class="fa fa-mail-reply mr-sm-2"></i>Quay lại</button></a>
                </div>
                <div class="card-body card-block">
                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                @csrf
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tên sản phấm</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="name" placeholder="Nhập tên sản phấm" class="form-control" value="{{ old ('name')}}"></div>
                @if($errors->has('name'))
                    <div style="color:red; margin-left: 320px">{{ $errors->first('name') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="select" class=" form-control-label">Danh mục</label></div>
                    <div class="col-12 col-md-9">
                        <select name="cate_id" id="select" class="form-control">
                            @foreach($list_cate as $cate)
                            <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="file-input" class=" form-control-label">Ảnh</label></div>
                    <div class="col-12 col-md-9"><input type="file" id="file-input" name="image" class="form-control-file"></div>
                @if($errors->has('image'))
                    <div style="color:red; margin-left: 320px">{{ $errors->first('image') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Giá sản phẩm</label></div>
                    <div class="col-12 col-md-9"><input type="number" id="text-input" name="price" placeholder="Nhập giá sản phấm" class="form-control" value="{{old ('price')}}"></div>
                @if($errors->has('price'))
                    <div style="color:red; margin-left: 320px">{{ $errors->first('price') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Sale</label></div>
                    <div class="col-12 col-md-9"><input type="number" id="text-input" name="sale_price" placeholder="Nhập giá khuyến mãi" class="form-control" value="{{old ('sale_price')}}"></div>
                @if($errors->has('sale_price'))
                    <div style="color:red; margin-left: 320px">{{ $errors->first('sale_price') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Mô tả</label></div>
                    <div class="col-12 col-md-9">
                    <textarea class="form-control" id="summary-ckeditor" name="description"></textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Chi tiết</label></div>
                    <div class="col-12 col-md-9">
                    <textarea class="form-control" id="summary-ckeditor" name="detail"></textarea>
                    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                    <script>
                    CKEDITOR.replace( 'detail' );
                    </script>
                    </div>
                </div>
                <div class="footer-add">
                        <input class="btn btn-success" type="submit" value="Thêm">
                </div>
                </form>
                <div class="footer-delete">
                <a href="add-sanpham.html">
                        <input class="btn btn-danger" type="submit" value="Hủy">
                    </a>
                </div>
                </div>  
            </div>
        </div>

    @endsection

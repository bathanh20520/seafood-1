@extends('backend.base')
@section('content')
        <!-- Phần viết code-->

        <div>
            <div class="card">
                <div class="card-header">
                    <strong>Thêm tin tức</strong>
                    <a href="{{route('tintuc')}}"><button type="submit" style="width: 100px"><i class="fa fa-mail-reply mr-sm-2"></i>Quay lại</button></a>
                </div>
                <div class="card-body card-block">
                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                @csrf
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Tiêu đề</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" value="{{old('title')}}" name="title" placeholder="Nhập tiêu đề" class="form-control"></div>
                @if($errors->has('title'))
                    <div style="color:red; margin-left: 285px">{{ $errors->first('title') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="select" class=" form-control-label">Danh mục tin</label></div>
                    <div class="col-12 col-md-9">
                        <select name="cate_post" id="select" class="form-control">
                            @foreach($list_cate as $cate)
                            <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="file-input" class=" form-control-label">Ảnh</label></div>
                    <div class="col-12 col-md-9"><input type="file" id="file-input" name="image" class="form-control-file"></div>
                @if($errors->has('image'))
                    <div style="color:red; margin-left: 285px">{{ $errors->first('image') }}</div>
                @endif
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Mô tả</label></div>
                    <div class="col-12 col-md-9">
                    <textarea class="form-control" id="summary-ckeditor" name="description"></textarea>
                    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                    <script>
                    CKEDITOR.replace( 'description' );
                    </script>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Chi tiết</label></div>
                    <div class="col-12 col-md-9">
                    <textarea class="form-control" id="summary-ckeditor" name="detail"></textarea>
                    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                    <script>
                    CKEDITOR.replace( 'detail' );
                    </script>
                    </div>
                </div>
                {{-- <div class="row form-group">
                    <div class="col col-md-3"><label for="select" class=" form-control-label">Tác giả</label></div>
                    <div class="col-12 col-md-9">
                        <select name="iduser" id="select" class="form-control">
                            @foreach($list_user as $user)
                            <option value="{{ $user->id }}">{{ $user->hoten }}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}
                <div class="card-footer">
                        <input class="btn btn-success" type="submit" value="Thêm">
                </div>
                </form>
                <div class="card-footer">
                    <a href="add-tintuc.html">
                        <input class="btn btn-danger" type="submit" value="Hủy">
                    </a>
                </div>
                </div>  
            </div>
        </div>

    @endsection
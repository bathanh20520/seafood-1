<?php

namespace App\Http\Controllers\back_end;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Requests\Danhmucsp;

class DanhmucController extends Controller
{
    public function Danhmuc(){
        $categorie= DB::table('categories_product')->paginate(5);
        return view('backend.categories.danhmuc',compact('categorie'));
    }

    public function Adddanhmuc(){
        return view('backend.categories.add-danhmuc');
    }

    public function Valiadd(Danhmucsp $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $danhmucsp = DB::table('categories_product')->insert($data);
        return redirect()->route('danhmuc')->with('success', 'Thêm danh mục thành công');
    }

    public function Editdanhmuc($id){
        $laydulieu = DB::table('categories_product')->find($id);
        return view('backend.categories.edit-danhmuc',compact('laydulieu'));
    }
    public function postEditdanhmuc($id, Danhmucsp $request){
        $data = $request->all();
        unset($data['_token']);
        DB::table('categories_product')
        ->where('id', $id)
        ->update($data);
        return redirect()->route('danhmuc')->with('success', 'Sửa danh mục thành công');
    }

    public function Deletedanhmuc($id){
        DB::table('products')->where('cate_id','=',$id)->delete();
        DB::table('categories_product')->delete($id);
        return redirect()->route('danhmuc')->with('success', 'Xóa danh mục thành công');
    }
}

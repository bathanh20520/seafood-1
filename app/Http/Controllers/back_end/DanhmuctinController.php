<?php
namespace App\Http\Controllers\back_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Requests\Danhmuctin;

class DanhmuctinController extends Controller
{
    public function Danhmuctt(){
        $dulieudanhmuctt= DB::table('categories_post')->paginate(5);
        return view('backend.cate_post.danhmuctt',compact('dulieudanhmuctt'));
    }

    public function Adddanhmuctt(){
        return view('backend.cate_post.add-danhmuctt');
    }

    public function Valiadd(Danhmuctin $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $danhmuctt = DB::table('categories_post')->insert($data);
        return redirect()->route('danhmuctt')->with('success', 'Thêm danh mục thành công');
    }

    public function Editdanhmuctt($id){
        $laydulieu = DB::table('categories_post')->find($id);
        return view('backend.cate_post.edit-danhmuctt',compact('laydulieu'));
    }
    public function postEditdanhmuctt($id, Danhmuctin $request){
        $data = $request->all();
        unset($data['_token']);
        DB::table('categories_post')
        ->where('id', $id)
        ->update($data);
        return redirect()->route('danhmuctt')->with('success', 'Sửa danh mục thành công');
    }

    public function Deletedanhmuctt($id){
        DB::table('tintuc')->where('iddm','=',$id)->delete();
        DB::table('categories_post')->delete($id);
        return redirect()->route('danhmuctt')->with('success', 'Xóa danh mục thành công');
    }
}
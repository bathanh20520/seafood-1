<?php

namespace App\Http\Controllers\back_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Requests\Slider;

use App\Http\Requests\EditSlide;

use Illuminate\Support\Facades\Log;

class SlideController extends Controller
{
    public function Slide(){
        $dulieuslide = DB::table('sliders')->paginate(3);
        return view('backend.slides.slide', compact('dulieuslide'));
    }

    public function change($id)
    {
        $user = DB::table('sliders')->find($id);
        if($user->status == 0){
            DB::table('sliders')->where('id','=',$id)->update(['status'=>1]);
        }else{
            DB::table('sliders')->where('id','=',$id)->update(['status'=>0]);
        }
        return redirect()->back();
    }
    
    public function Addslide(){
        return view('backend.slides.add-slide');
    }

    public function Editslide($id){
        $data = DB::table('sliders')->find($id);
        return view('backend.slides.edit-slide',compact('data'));
    }

    public function postEditslide($id, EditSlide $request){
        $data = $request->all();
        unset($data['_token']);
        $file = $request->image;
        if(isset($file)){
            $url_img=$file->move('upload/anh_sp', $file->getClientOriginalName());
            $data['image'] = $url_img;
        }else{
            $thongtinslide = DB::table('sliders')->find($id);
            $data['image'] = $thongtinslide->image;
        }
        DB::table('sliders')
        ->where('id', $id)
        ->update($data);
        return redirect()->route('slider')->with('success', 'Sửa slide thành công');
    }

    public function Deleteslide($id){
        DB::table('sliders')->delete($id);
        return redirect()->route('slider')->with('success', 'Xóa slide thành công');
    }

    public function Valiadd(Slider $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $file = $request->image;
        $url_img=$file->move('upload/anh_sp', $file->getClientOriginalName());
        $data['image'] = $url_img;
        $slide = DB::table('sliders')->insert($data);
        return redirect()->route('slider')->with('success', 'Thêm slide thành công');
    }
}

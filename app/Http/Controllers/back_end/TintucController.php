<?php

namespace App\Http\Controllers\back_end;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Requests\Tintuc;

use App\Http\Requests\SuaTinTuc;

class TintucController extends Controller
{
    public function Tintuc(){
        $dulieutintuc= DB::table('post')->paginate(5);
        $list_cate = DB::table('categories_post')->get();
        // $list_user = DB::table('users')->get();
        return view('backend.posts.tintuc', compact('dulieutintuc','list_cate',));
    }

    public function Addtintuc(){
        $list_cate = DB::table('categories_post')->get();
        // $list_user = DB::table('users')->get();
        return view('backend.posts.add-tintuc',compact('list_cate'));
    }

    public function Valiaddtt(Tintuc $request)
    {
        $data = $request->all();
        unset($data['_token']);
        // $data['soluongban']=0;
        $data['views']=0;
        $file = $request->image;
        $url_img=$file->move('upload/anh_tt', $file->getClientOriginalName());
        $data['image'] = $url_img;
        $tintuc = DB::table('post')->insert($data);
        return redirect()->route('tintuc')->with('success', 'Thêm tin tức thành công');
    }

    public function Edittintuc($id){
        $laydulieu = DB::table('post')->find($id);
        $list_cate = DB::table('categories_post')->get();
        // $list_user = DB::table('users')->get();
        return view('backend.posts.edit-tintuc',compact('laydulieu','list_cate'));
    }

    public function postEdittintuc($id, SuaTinTuc $request){
        $data = $request->all();
        unset($data['_token']);
        $file = $request->image;
        if(isset($file)){
            $url_img=$file->move('upload/anh_tt', $file->getClientOriginalName());
            $data['image'] = $url_img;
        }else{
            $thongtintt = DB::table('post')->find($id);
            $data['image'] = $thongtintt->image;
        }
        
        $tintuc = DB::table('post')
        ->where('id', $id)
        ->update($data);
        return redirect()->route('tintuc')->with('success', 'Sửa tin tức thành công');
    }

    public function Deletetintuc($id){
        DB::table('post')->delete($id);
        return redirect()->route('tintuc')->with('success', 'Xóa tin tức thành công');
    }

    
}

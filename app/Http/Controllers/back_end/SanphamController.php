<?php

namespace App\Http\Controllers\back_end;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Requests\Sanpham;

use App\Http\Requests\SuaSanPham;

class SanphamController extends Controller
{
    public function Sanpham(){
        $products= DB::table('products')->paginate(5);
        $list_cate = DB::table('categories_product')->get();
        return view('backend.products.sanpham', compact('products','list_cate'));
    }

    public function Addsanpham(){
        $list_cate = DB::table('categories_product')->get();
        return view('backend.products.add-sanpham',compact('list_cate'));
    }

    public function Valiaddsp(Sanpham $request)
    {
        $data = $request->all();
        unset($data['_token']);
        // $data['unti']=0;
        $file = $request->image;
        $url_img=$file->move('upload/anh_sp', $file->getClientOriginalName());
        $data['image'] = $url_img;
        $sanpham = DB::table('products')->insert($data);
        return redirect()->route('sanpham')->with('success', 'Thêm sản phẩm thành công');
    }

    public function Editsanpham($id){
        $products = DB::table('products')->find($id);
        $list_cate = DB::table('categories_product')->get();
        return view('backend.products.edit-sanpham',compact('products','list_cate'));
    }

    public function postEditsanpham($id, SuaSanPham $request){
        $data = $request->all();
        unset($data['_token']);
        $file = $request->image;
        if(isset($file)){
            $url_img=$file->move('upload/anh_sp', $file->getClientOriginalName());
            $data['image'] = $url_img;
        }else{
            $thongtinsp = DB::table('products')->find($id);
            $data['image'] = $thongtinsp->image;
        }
        
        $sanpham = DB::table('products')
        ->where('id', $id)
        ->update($data);
        return redirect()->route('sanpham')->with('success', 'Sửa sản phẩm thành công');
    }

    public function Deletesanpham($id){
        DB::table('products')->delete($id);
        return redirect()->route('sanpham')->with('success', 'Xóa sản phẩm thành công');
    }


}
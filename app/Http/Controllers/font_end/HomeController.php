<?php

namespace App\Http\Controllers\font_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(){
        return view('font_end.index');
    }
}

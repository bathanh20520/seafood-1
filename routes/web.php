<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Font-End 
Route::group(['prefix'=> '/'], function(){
    Route::get('/','font_end\HomeController@index')->name('font.index');
    Route::get('san-pham', 'font_end\ProductController@listProduct')->name('font.product');
    Route::get('tin-tuc', 'font_end\NewController@listNew')->name('font.new');
    Route::get('gioi-thieu', 'font_end\IntroduceController@listIntro')->name('font.intro');
    Route::get('lien-he', 'font_end\ContactController@listContact')->name('font.contact');
    Route::get('chi-tiet', 'font_end\ProductController@productDetail')->name('font.detail');
    Route::get('gio-hang', 'font_end\CartController@listCart')->name('font.cart');
    
});

//Back-End
Route::group(['prefix'=>'/'],function(){
    Route::get('admin', 'back_end\BaseController@Admin')->name('admin');

    // Danh mục
    Route::get('quantridanhmuc','back_end\DanhmucController@Danhmuc')->name('danhmuc');
    Route::get('add-danhmuc', 'back_end\DanhmucController@Adddanhmuc' )->name('adddanhmuc');
    Route::post('add-danhmuc', 'back_end\DanhmucController@Valiadd' )->name('postdanhmuc');
    Route::get('edit-danhmuc/{id}','back_end\DanhmucController@Editdanhmuc')->name('editdanhmuc');
    Route::post('edit-danhmuc/{id}','back_end\DanhmucController@postEditdanhmuc')->name('posteditdanhmuc');
    Route::get('delete-danhmuc/{id}','back_end\DanhmucController@Deletedanhmuc')->name('deletedanhmuc');
});
// Sản phẩm
    Route::get('quantrisanpham', 'back_end\SanphamController@Sanpham' )->name('sanpham');
    Route::get('add-sanpham', 'back_end\SanphamController@Addsanpham' )->name('addsanpham');
    Route::post('add-sanpham', 'back_end\SanphamController@Valiaddsp' )->name('postsanpham');
    Route::get('edit-sanpham/{id}', 'back_end\SanphamController@Editsanpham' )->name('editsanpham');
    Route::post('edit-sanpham/{id}','back_end\SanphamController@postEditsanpham')->name('posteditsanpham');
    Route::get('delete-sanpham/{id}','back_end\SanphamController@Deletesanpham')->name('deletesanpham');

    //Danh mục tin tức
    Route::get('quantridanhmuctt','back_end\DanhmuctinController@Danhmuctt')->name('danhmuctt');
    Route::get('add-danhmuctt', 'back_end\DanhmuctinController@Adddanhmuctt' )->name('adddanhmuctt');
    Route::post('add-danhmuctt', 'back_end\DanhmuctinController@Valiadd' )->name('postdanhmuctt');
    Route::get('edit-danhmuctt/{id}','back_end\DanhmuctinController@Editdanhmuctt')->name('editdanhmuctt');
    Route::post('edit-danhmuctt/{id}','back_end\DanhmuctinController@postEditdanhmuctt')->name('posteditdanhmuctt');
    Route::get('delete-danhmuctt/{id}','back_end\DanhmuctinController@Deletedanhmuctt')->name('deletedanhmuctt');
    
    //Tin tức
     Route::get('quantritintuc','back_end\TintucController@Tintuc')->name('tintuc');
     Route::get('add-tintuc', 'back_end\TintucController@Addtintuc' )->name('addtintuc');
     Route::post('add-tintuc', 'back_end\TintucController@Valiaddtt' )->name('posttintuc');
     Route::get('edit-tintuc/{id}','back_end\TintucController@Edittintuc')->name('edittintuc');
     Route::post('edit-tintuc/{id}','back_end\TintucController@postEdittintuc')->name('postedittintuc');
     Route::get('delete-tintuc/{id}','back_end\TintucController@Deletetintuc')->name('deletetintuc');
 
     // Slider
    Route::get('/quantrislide', 'back_end\SlideController@Slide' )->name('slider');
    Route::get('/quantrislide/{id}', 'back_end\SlideController@change' );
    Route::get('/add-slide', 'back_end\SlideController@Addslide' )->name('addslide');
    Route::post('/add-slide', 'back_end\SlideController@Valiadd' )->name('postslide');
    Route::get('/edit-slide/{id}', 'back_end\SlideController@Editslide' )->name('editslide');
    Route::post('/edit-slide/{id}','back_end\SlideController@postEditslide')->name('posteditslide');
    Route::get('/delete-slide/{id}','back_end\SlideController@Deleteslide')->name('deleteslide');
   
    //Login - Logout (User)
Route::group(['prefix'=>'auth'],function(){
    Route::get('dang-ky', 'auth\AuthController@register')->name('auth.register');
    Route::get('dang-nhap', 'auth\AuthController@login')->name('auth.login');
});

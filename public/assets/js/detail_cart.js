var clear = document.getElementsByClassName("delete-cart");

for (var i = 0; i < clear.length; i++) {
  clear[i].addEventListener("click", function (event) {
    event.preventDefault();
    this.closest(".table-body").classList.remove("active");

    if (document.querySelectorAll(".table-body.active").length == 0) {
      this.closest(".cart-detail").classList.remove("active");
      document.getElementById("cart-detail-empty").classList.add("active");
    }
  });
}

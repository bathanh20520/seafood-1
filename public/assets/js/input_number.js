// input[type="number"]

$(document).ready(function () {
  jQuery(
    '<div class="quantity-nav"><button class="quantity-button quantity-down">-</button><button class="quantity-button quantity-up">+</button></div>'
  ).insertAfter(".number-pro input");
  jQuery(".number-pro").each(function () {
    var spinner = jQuery(this),
      input = spinner.find('input[type="number"]'),
      btnUp = spinner.find(".quantity-up"),
      btnDown = spinner.find(".quantity-down"),
      min = input.attr("min"),
      max = input.attr("max");

    btnUp.click(function () {
      var oldValue = parseFloat(input.val());
      if (oldValue >= max) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue + 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

    btnDown.click(function () {
      var oldValue = parseFloat(input.val());
      if (oldValue <= min) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue - 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });
  });
});

var inputRadio = document.querySelectorAll(".select-info input[type='radio']");
for (var i = 0; i < inputRadio.length; i++) {
  inputRadio[i].addEventListener("click", function () {
    for (var i = 0; i < inputRadio.length; i++) {
      inputRadio[i].closest("label").classList.remove("active");
    }

    this.closest("label").classList.add("active");
  });
}
